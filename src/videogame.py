from enum import Enum


class Videogames(Enum):
    FalloutNewVegas = 0
    ZeldaBreathOfTheWild = 1
    FinalFantasyTactics = 2
    AgeOfEmpiresII = 3
    LeagueOfLegends = 4
    SuperSmashBrothersMelee = 5
    ET = 6
    Fallout76 = 7
    Skyrim = 8
    CounterStrike = 9


GOOD_VIDEOGAMES = [
    Videogames.FalloutNewVegas,
    Videogames.ZeldaBreathOfTheWild,
    Videogames.FinalFantasyTactics,
    Videogames.AgeOfEmpiresII,
    Videogames.SuperSmashBrothersMelee,
    Videogames.Skyrim
]

def is_good(value):
    return value in GOOD_VIDEOGAMES


class Person:
    def __init__(self, name, liked_games):
        self.name = name
        self.liked_games = liked_games

    def __str__(self):
        return self.name

    def likes(self, value):
        return value in self.liked_games


Guilherme = Person("Guilherme", GOOD_VIDEOGAMES)
Gabriel = Person("Gabriel", GOOD_VIDEOGAMES + [Videogames.LeagueOfLegends])
Tito = Person("Tito", GOOD_VIDEOGAMES)




