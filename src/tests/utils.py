import itertools
from typing import List, Tuple, Callable, Union

import pytest


def combine_params(*args: List[List]):
    return list(itertools.product(*args))


def indirect_fixture_getter(data_fixture, request):
    return getattr(data_fixture, request.param)


class InvalidFixtureAlias(Exception):
    pass


def mark_parametrize_with_fixture(
    *args, indirect: Union[bool, List[Tuple[str, Callable]]] = False, **kwargs
):

    if not isinstance(indirect, list):
        return pytest.mark.parametrize(*args, indirect=indirect, **kwargs)

    final_indirect = []
    for entry in indirect:
        if isinstance(entry, Tuple) and len(entry) == 2:
            alias = entry[0]

            final_indirect.append(alias)
        else:
            final_indirect.append(entry)

    def func_wrapper(func):
        g: dict = func.__globals__

        for entry in indirect:
            if isinstance(entry, Tuple) and len(entry) == 2:
                alias = entry[0]
                fixture = entry[1]

                if g.get(alias) is not None:
                    raise InvalidFixtureAlias(
                        f"'{alias}' is already a fixture declared for the test's scope. Please change the param name."
                    )
                g[alias] = fixture

        return pytest.mark.parametrize(*args, indirect=final_indirect, **kwargs)(func)

    return func_wrapper