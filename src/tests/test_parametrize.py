import itertools

import pytest

from src.tests.utils import combine_params
from src.videogame import Videogames, is_good, Guilherme, Gabriel, Tito


class TestIsVideogameGood:

    def test_falloutnewvegas_good(self):
        assert is_good(Videogames.FalloutNewVegas)

    def test_breathofthewild_good(self):
        assert is_good(Videogames.ZeldaBreathOfTheWild)

    def test_leagueoflegends_bad(self):
        assert not is_good(Videogames.LeagueOfLegends)

    def test_smashbros_good(self):
        assert is_good(Videogames.SuperSmashBrothersMelee)

    @pytest.mark.parametrize(
        "videogame",
        [
            Videogames.FalloutNewVegas,
            Videogames.ZeldaBreathOfTheWild,
            Videogames.SuperSmashBrothersMelee
        ]
    )
    def test_videogame_good(self, videogame):
        assert is_good(videogame)

    @pytest.mark.parametrize(
        "videogame",
        [
            Videogames.LeagueOfLegends,
            Videogames.Fallout76,
            Videogames.ET
        ]
    )
    def test_videogame_bad(self, videogame):
        assert not is_good(videogame)


class TestPersonLikesVideogame:

    @pytest.mark.parametrize(
        "person",
        [
            Guilherme,
            Gabriel
        ]
    )
    def test_person_dislikes_badgames(self, person):
        bad_games = [
            Videogames.ET,
            Videogames.Fallout76,
            Videogames.LeagueOfLegends
        ]

        for game in bad_games:
            assert not person.likes(game)

    @pytest.mark.parametrize(
        "person,bad_game",
        [
            (Guilherme, Videogames.ET),
            (Guilherme, Videogames.Fallout76),
            (Guilherme, Videogames.LeagueOfLegends),
            (Gabriel, Videogames.ET),
            (Gabriel, Videogames.Fallout76),
            (Gabriel, Videogames.LeagueOfLegends),
        ]
    )
    def test_person_dislikes_badgame(self, person, bad_game):
        assert not person.likes(bad_game)

    @pytest.mark.parametrize(
        "person,bad_game",
        list(itertools.product(
            [
                Guilherme,
                Tito
            ],
            [
                Videogames.ET,
                Videogames.Fallout76,
                Videogames.LeagueOfLegends
            ]))
    )
    def test_person_dislikes_badgame_itertools(self, person, bad_game):
        assert not person.likes(bad_game)

    @pytest.mark.parametrize(
        "person,bad_game",
        combine_params(
            [
                Guilherme,
                Tito
            ],
            [
                Videogames.ET,
                Videogames.Fallout76,
                Videogames.LeagueOfLegends
            ])
    )
    def test_person_dislikes_badgame_combineparams(self, person, bad_game):
        assert not person.likes(bad_game)

