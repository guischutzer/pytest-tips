import pytest
from hypothesis import given, strategies
from hypothesis.strategies import sampled_from
from pytest_lazyfixture import lazy_fixture, LazyFixture

from src.tests.utils import combine_params, mark_parametrize_with_fixture
from src.videogame import Person, GOOD_VIDEOGAMES, Videogames


# Fixtures

class VideogameData:

    def __init__(self):
        self.guilherme = Person(
            name="Guilherme",
            liked_games=GOOD_VIDEOGAMES
        )

        self.gabriel = Person(
            name="Gabriel",
            liked_games=GOOD_VIDEOGAMES + [Videogames.LeagueOfLegends]
        )

        self.tito = Person(
            name="Tito",
            liked_games=[Videogames.AgeOfEmpiresII, Videogames.Skyrim]
        )

        self.older_gabriel = Person(
            name="Gabriel",
            liked_games=GOOD_VIDEOGAMES
        )

        self.et = Videogames.ET
        self.lol = Videogames.LeagueOfLegends


@pytest.fixture
def videogame_data():
    return VideogameData()


def test_gabriel_likes_lol(videogame_data):
    assert videogame_data.gabriel.likes(Videogames.LeagueOfLegends)


# Indirect parameters (all)


@pytest.fixture
def gabriel_in_year(request, videogame_data):
    if request.param > 2022:
        return videogame_data.older_gabriel

    return videogame_data.gabriel


# this should fail
@pytest.mark.parametrize(
    "gabriel_in_year",
    [
        2009,
        2015,
        2022,
        2023
    ],
    indirect=True
)
def test_gabriel_likes_lol_in_year(gabriel_in_year):
    assert gabriel_in_year.likes(Videogames.LeagueOfLegends)


# Indirect parameters (by name)


# this should work
@pytest.mark.parametrize(
    "gabriel_in_year, expected",
    [
        (2009, True),
        (2015, True),
        (2022, True),
        (2023, False)
    ],
    indirect=["gabriel_in_year"]
)
def test_gabriel_likes_lol_in_year(gabriel_in_year, expected):
    assert gabriel_in_year.likes(Videogames.LeagueOfLegends) == expected


# Accessing fixture elements from parameters


#error
@pytest.mark.parametrize(
    "person,bad_game",
    [
        (videogame_data.guilherme, Videogames.ET),
        (Guilherme, Videogames.Fallout76),
        (Guilherme, Videogames.LeagueOfLegends),
        (Gabriel, Videogames.ET),
        (Gabriel, Videogames.Fallout76),
        (Gabriel, Videogames.LeagueOfLegends),
    ]
)
def test_person_dislikes_badgame_fixture(person, bad_game):
    assert not person.likes(bad_game)

@pytest.fixture
def person(request, videogame_data):
    return getattr(videogame_data, request.param)


@pytest.mark.parametrize(
    "person,bad_game",
    [
        ("guilherme", Videogames.ET),
        ("guilherme", Videogames.Fallout76),
        ("guilherme", Videogames.LeagueOfLegends),
        ("gabriel", Videogames.ET),
        ("gabriel", Videogames.Fallout76),
        ("older_gabriel", Videogames.LeagueOfLegends),
    ],
    indirect=["person"]
)
def test_person_dislikes_badgame_fixture_indirect(person, bad_game):
    assert not person.likes(bad_game)

# Tip: you can use any helper alongside with indirect, only thing
# that matters is the resulting list of parameters

@pytest.mark.parametrize(
    "person,bad_game",
    combine_params(
        ["guilherme", "older_gabriel"],
        [Videogames.ET, Videogames.Fallout76, Videogames.LeagueOfLegends]
    ),
    indirect=["person"]  # here can be changed to videogame_data_param
)
def test_person_dislikes_badgame_fixture_indirect_combine(person, bad_game):
    assert not person.likes(bad_game)

@pytest.fixture
def get_videogame_data(request, videogame_data):
    return getattr(videogame_data, request.param)

videogame_person = get_videogame_data
bad_game = get_videogame_data

@pytest.mark.parametrize(
    "videogame_person,bad_game",
    combine_params(
        ["guilherme", "older_gabriel"],
        ["et", "lol"]
    ),
    indirect=True
)
def test_person_dislikes_badgame_fixture_indirect_combine_2(videogame_person, bad_game):
    assert not videogame_person.likes(bad_game)

@mark_parametrize_with_fixture(
    "videogame_person_2,bad_game_2",
    combine_params(
        ["guilherme", "older_gabriel"],
        ["et", "lol"]
    ),
    indirect=[("videogame_person_2", get_videogame_data), ("bad_game_2", get_videogame_data)]
)
def test_person_dislikes_badgame_custom_mark(videogame_person_2, bad_game_2):
    assert not videogame_person_2.likes(bad_game_2)

@pytest.fixture
def guilherme_fixture(videogame_data):
    return videogame_data.guilherme

@pytest.fixture
def gabriel_fixture(videogame_data):
    return videogame_data.gabriel

@pytest.mark.parametrize(
    "person",
    [
        lazy_fixture("guilherme_fixture"),
        lazy_fixture("gabriel_fixture")
    ]
)
def test_lazy_fixture(person):
    assert person.name != "Tito"
